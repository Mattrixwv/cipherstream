//Ciphers/main.cpp
//Matthew Ellison
// Created: 04-25-18
//Modified: 03-07-19
//This file contains the driver function and the test functions for the Cipher program
//This program will use some simple ciphers that are no longer widely used but still fun to play with
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//The header files
#include "Headers/Caesar.hpp"
#include "Headers/Playfair.hpp"
#include "Headers/Vigenere.hpp"
#include "Headers/Atbash.hpp"
#include "Headers/Morse.hpp"
#include <iostream>
#include <string>

//If this is the test version include the appropriate functions from the file
#ifdef TEST_VERSION
#include "testHandler.hpp"	//This inserts the main function with all of the test functions
#else
//We need these files for normal opperations
#include "helperFunctions.hpp"
#include <fstream>
#endif //TEST_VERSION


int main(int argc, char** argv){
	//If this is the test version of the program run the test function
	#ifdef TEST_VERSION
	testHandler(argc, argv);
	//Otherwise handle the program normally
	#else	//TEST_VERSION
	bool cipherFlags[SIZE];
	std::string inputFileName, outputFileName, cipherString;
	std::ifstream inputFile;
	std::ofstream outputFile;

	//Make sure the flags are all false by default
	for(int cnt = 0;cnt < SIZE;++cnt){
		cipherFlags[cnt] = false;
	}

	//Set the flags and test for errors
	try{
		getFlags(argc, argv, cipherFlags, inputFileName, outputFileName);
	}
	//Catch any errors that were given
	catch(noFileName error){
		std::cout << "The " << error.getType() << " file flag was set but no file name was given" << std::endl;
		return 1;
	}
	catch(invalidFlag error){
		std::cout << '-' << error.getFlag() << " is not a valid flag" << std::endl;
		return 1;
	}
	catch(invalidCombo error){
		std::cout << "An invalid combination of flags was given where " << error.getType() << std::endl;
		return 1;
	}

	//Check if output file can be opened
	if(cipherFlags[OUTPUT_FILE]){
		outputFile.open(outputFileName);
		if(outputFile.fail()){
			std::cout << "The output file could not be opened" << std::endl;
			return 0;
		}
	}
	//Check if input file can be opened
	if(cipherFlags[INPUT_FILE]){
		inputFile.open(inputFileName);
		if(inputFile.fail()){
			std::cout << "The input file could not be opened" << std::endl;
			return 0;
		}
	}

	//Run the appropriate functions for the cipher
	Fn cipherFunction = getCipher(cipherFlags);
	cipherString = cipherFunction(inputFile, cipherFlags[ENCODE]);

	if(failFlag){
		std::cout << cipherString << std::endl;
		return 0;
	}

	//Decide where the cipher will be output
	if(cipherFlags[OUTPUT_FILE]){
		outputFile << cipherString << std::endl;
	}
	else{
		std::cout << cipherString << std::endl;
	}

	inputFile.close();
	outputFile.close();
	#endif	//TEST_VERSION
	return 0;
}
