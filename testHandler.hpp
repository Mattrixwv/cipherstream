//Ciphers/testMain.hpp
//Matthew Ellison
// Created: 04-30-18
//Modified: 03-07-19
//This file contains the driver function and the test functions for the Cipher program
//This program will use some simple ciphers that are no longer widely used but still fun to play with
/*
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Headers/Caesar.hpp"
#include "Headers/Playfair.hpp"
#include "Headers/Vigenere.hpp"
#include "Headers/Atbash.hpp"
#include "Headers/Morse.hpp"
#include "Headers/Autokey.hpp"
#include <iostream>
#include <string>


//#define TEST_VERSION

#ifdef TEST_VERSION
#define CAESAR_TEST
#define PLAYFAIR_TEST
#define VIGENERE_TEST
#define ATBASH_TEST
#define MORSE_TEST
#define AUTOKEY_TEST
/**
 * @brief Helper for determining the state of a cipher when an error occured
 * 
 */
enum codingState { ENCODE, DECODE };
/**
 * @brief Creates an error message
 * 
 * @param type Determines whether the cipher was encoding or decoding when an error occured
 * @param input The string that was input into the cipher
 * @param output The string that was the expected output of the cipher
 * @param cipher The string that was the actual output of the cipher
 * @return The appropriate error message
 */
std::string testingError(codingState type, const std::string& input, const std::string& output, const std::string& cipher);
#endif //TEST_VERSION definition

#ifdef CAESAR_TEST
/**
 * @brief Runs several tests on the Caesar cipher, checking if encoding and decoding are working propperly
 * 
 * @param errorString A string to store any error messages
 * @return true If all tests completed successfully
 * @return false If 1 or more tests failed
 */
bool caesarTest(std::string& errorString);
#endif //CAESAR_TEST

#ifdef PLAYFAIR_TEST
/**
 * @brief Runs several tests on the Playfair cipher, checking if encoding and decoding are working propperly
 * 
 * @param errorString A string to store any error messages
 * @return true If all tests completed successfully
 * @return false If 1 or more tests failed
 */
bool playfairTest(std::string& errorString);
#endif //PLAYFAIR_TEST

#ifdef VIGENERE_TEST
/**
 * @brief Runs several tests on the Vigenere cipher, checking if encoding and decoding are working propperly
 * 
 * @param errorString A string to store any error messages
 * @return true If all tests completed successfully
 * @return false If 1 or more tests failed
 */
bool vigenereTest(std::string& errorString);
#endif //VIGENERE_TEST

#ifdef ATBASH_TEST
/**
 * @brief Runs several tests on the Atbash cipher, checking if encoding and decoding are working propperly
 * 
 * @param errorString A string to store any error messages
 * @return true If all tests completed successfully
 * @return false If 1 or more tests failed
 */
bool atbashTest(std::string& errorString);
#endif //ATBASH_TEST

#ifdef MORSE_TEST
/**
 * @brief Runs several tests on Morse code, checking if encoding and decoding are working propperly
 * 
 * @param errorString A string to store any error messages
 * @return true If all tests completed successfully
 * @return false If 1 or more tests failed
 */
bool morseTest(std::string& errorString);
#endif //MORSE_TEST

#ifdef AUTOKEY_TEST
/**
 * @brief Runs several tests on the Autokey cipher, checking if encoding and decoding are working propperly
 * 
 * @param errorString A string to store any error messages
 * @return true If all tests completed successfully
 * @return false If 1 or more tests failed
 */
bool autokeyTest(std::string& errorString);
#endif //AUTOKEY_TEST


#ifdef TEST_VERSION
//A different main function that facilitates all tests
void testHandler(int argc, char** argv){
	bool testResult = false;
	std::string resultString = "";
	std::string errorString = "";

	//Testing the Caesar Cipher
	#ifdef CAESAR_TEST
	testResult = caesarTest(errorString);
	//If the test was successful
	if(testResult){
		resultString += "Caesar Cipher completed successfully\n";
	}
	else{
		resultString += "Caesar Cipher error in " + errorString;
	}
	//Reset the variables
	errorString = "";
	testResult = false;
	#endif //CAESAR_TEST

	//Testing the playfair cipher
	#ifdef PLAYFAIR_TEST
	testResult = playfairTest(errorString);
	if(testResult){
		resultString += "Playfair Cipher completed successfully\n";
	}
	else{
		resultString += "Playfair Cipher error in " + errorString;
	}
	errorString = "";
	testResult = false;
	#endif //PLAYFAIR_TEST

	#ifdef VIGENERE_TEST
	testResult = vigenereTest(errorString);
	if(testResult){
		resultString += "Vigenere Cipher completed successfully\n";
	}
	else{
		resultString += "Vigenere Cipher error in " + errorString;
	}
	errorString = "";
	testResult = false;
	#endif //VIGENERE_TEST

	#ifdef ATBASH_TEST
	testResult = atbashTest(errorString);
	if(testResult){
		resultString += "Atbash Cipher completed successfully\n";
	}
	else{
		resultString += "Atbash Cipher error in " + errorString;
	}
	errorString = "";
	testResult = false;
	#endif //ATBASH_TEST

	#ifdef MORSE_TEST
	testResult = morseTest(errorString);
	if(testResult){
		resultString += "Morse Code completed successfully\n";
	}
	else{
		resultString += "Morse Code error in " + errorString;
	}
	errorString = "";
	testResult = false;
	#endif //MORSE_TEST

	#ifdef AUTOKEY_TEST
	testResult = autokeyTest(errorString);
	if(testResult){
		resultString += "Autokey Cipher completed successfully\n";
	}
	else{
		resultString += "Autokey Cipher error in " + errorString;
	}
	errorString = "";
	testResult = false;
	#endif //AUTOKEY_TEST

	//Print the results
	std::cout << "Results:\n" << resultString << std::endl;
}

std::string testingError(codingState type, const std::string& input, const std::string& output, const std::string& cipher){
	std::string errorMessage;
	//Decide if it was encoding or decoding
	if(type == codingState::ENCODE){
		errorMessage = "Encoding:";
	}
	else if(type == DECODE){
		errorMessage = "Decoding:";
	}
	else{
		errorMessage = "We gots problems";
	}
	errorMessage += "\nError in: " + input + "\nExpected: " + output + "\nActual:   " + cipher + '\n';

	return errorMessage;
}


#ifdef CAESAR_TEST
bool caesarTest(std::string& errorString){
	///Add something in error message about shift amount
	Caesar cipher;
	bool passed = true;
	std::string inputString;	//Holds the string to be input into the cipher
	std::string outputString;	//Holds the string that the cipher is expected to output
	std::string cipherString;	//Holds the string that the cipher actually output

	///All lowercase
	inputString = "abcdefghijklmnopqrstuvwxyz";	//Shift = 3
	outputString = "defghijklmnopqrstuvwxyzabc";	//Shift = 3
	//Try it forwards
	cipherString = cipher.encode(3, inputString);
	if(cipherString != outputString){
		//errorString += "Encoding:\nError in: " + inputString + "\nExpected: " + outputString + "\nActual:   " + cipherString + '\n';
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//Try it backwards
	cipherString = cipher.decode(3, outputString);
	if(cipherString != inputString){
		//errorString += "Decoding:\nError in: " + outputString + "\nExpected: " + inputString + "\nActual:   " + cipherString + '\n';
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	///All upercase
	inputString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	outputString = "XYZABCDEFGHIJKLMNOPQRSTUVW";
	//Try it forwards
	cipherString = cipher.encode(-3, inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//Try it backwards
	cipherString = cipher.decode(-3, outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	///Both cases
	inputString = "AbCdEfGhIjKlMnOpQrStUvWxYz";
	outputString = "DeFgHiJkLmNoPqRsTuVwXyZaBc";
	//Try it forwards
	cipherString = cipher.encode(3, inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//Try it backwards
	cipherString = cipher.decode(3, outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	///Punctuation
	inputString = "abc,def. rst; wxyz";
	outputString = "def,ghi. uvw; zabc";
	//Try it forwards
	cipherString = cipher.encode(3, inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//Try it backwards
	cipherString = cipher.decode(3, outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	inputString = "you";
	outputString = "iye";
	//Try it forwards
	cipherString = cipher.encode(10, inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//Try it backwards
	cipherString = cipher.decode(10, outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	return passed;
}
#endif //CAESAR_TEST

#ifdef PLAYFAIR_TEST
bool playfairTest(std::string& errorString){
	bool passed = true;
	std::string keyword, inputString, outputString, cipherString;
	Playfair cipher;

	//Test from wikipedia
	keyword = "Playfair Example";
	inputString = "Hide the gold in the tree stump";
	outputString = "BMODZBXDNABEKUDMUIXMMOUVIF";
	//Setup the cipher
	cipherString = cipher.encode(keyword, inputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This removes the spaces and turns everything capital so the reverse will work correctly
	inputString = cipher.getInputString();

	//Do the same thing in reverse
	cipher.reset();
	cipherString = cipher.decode(keyword, outputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	//Test from wikipedia
	keyword = "Playfair Example";
	inputString = "Hide the gold in the tree stum";
	outputString = "BMODZBXDNABEKUDMUIXMMOUVIM";
	//Setup the cipher
	cipherString = cipher.encode(keyword, inputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This removes the spaces and turns everything capital so the reverse will work correctly
	inputString = cipher.getInputString();

	//Do the same thing in reverse
	cipher.reset();
	cipherString = cipher.decode(keyword, outputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	//Return if the cipher passed the tests
	return passed;
}
#endif //PLAYFAIR_TEST

#ifdef VIGENERE_TEST
bool vigenereTest(std::string& errorString){
	bool passed = true;
	std::string keyword, inputString, outputString, cipherString;
	Vigenere cipher;

	//Test from wikipedia
	keyword = "lemon";
	inputString = "Attack at dawn";
	outputString = "LXFOPVEFRNHR";
	//Setup the cipher
	cipherString = cipher.encode(keyword, inputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This removes the spaces and turns everything capital so the reverse will work correctly
	inputString = cipher.getInputString();

	//Do the same thing in reverse
	cipher.reset();
	cipherString = cipher.decode(keyword, outputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}


	//Second test from wikipedia
	keyword = "ABCD";
	inputString = "CRYPTOISSHORTFORCRYPTOGRAPHY";
	outputString = "CSASTPKVSIQUTGQUCSASTPIUAQJB";
	//Setup the cipher
	cipherString = cipher.encode(keyword, inputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This removes the spaces and turns everything capital so the reverse will work correctly
	inputString = cipher.getInputString();

	//Do the same thing in reverse
	cipher.reset();
	cipherString = cipher.decode(keyword, outputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}


	//Return if the cipher passed the tests
	return passed;
}
#endif //VIGENERE_TEST

#ifdef ATBASH_TEST
bool atbashTest(std::string& errorString){
	bool passed = true;
	std::string inputString, outputString, cipherString;
	Atbash cipher;

	inputString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	outputString = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
	cipherString = cipher.encode(inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}

	//Try it backwards
	cipher.reset();
	cipherString = cipher.decode(outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	return passed;
}
#endif //ATBASH_TEST

#ifdef MORSE_TEST
bool morseTest(std::string& errorString){
	bool passed = true;
	std::string inputString, outputString, cipherString;
	Morse cipher;

	inputString = "SOS";
	outputString = "... --- ...";
	cipherString = cipher.encode(inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}

	//Try it backwards
	cipher.reset();
	cipherString = cipher.decode(outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}


	inputString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	outputString = ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --.. ----- .---- ..--- ...-- ....- ..... -.... --... ---.. ----.";
	cipherString = cipher.encode(inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}

	//Try it backwards
	cipher.reset();
	cipherString = cipher.decode(outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}


	inputString = "Matthew Ellison: 960-9775";
	outputString = "-- .- - - .... . .-- . .-.. .-.. .. ... --- -. ----. -.... ----- ----. --... --... .....";
	cipherString = cipher.encode(inputString);
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This sterilizes the input
	inputString = cipher.getInputString();

	//Try it backwards
	cipher.reset();
	cipherString = cipher.decode(outputString);
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	return passed;
}
#endif //MORSE_TEST

#ifdef AUTOKEY_TEST
bool autokeyTest(std::string& errorString){
	bool passed = true;
	std::string keyword, inputString, outputString, cipherString;
	Autokey cipher;

	//Test from wikipedia
	keyword = "QUEENLY";
	inputString = "Attack at dawn";
	outputString = "QNXEPVYTWTWP";
	//Setup the cipher
	cipherString = cipher.encode(keyword, inputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This removes the spaces and turns everything capital so the reverse will work correctly
	inputString = cipher.getInputString();

	//Do the same thing in reverse
	cipher.reset();
	cipherString = cipher.decode(keyword, outputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}


	//Another test from wikipedia
	keyword = "KILT";
	inputString = "MEETATTHEFOUNTAIN";
	outputString = "WMPMMXXAEYHBRYOCA";
	//Setup the cipher
	cipherString = cipher.encode(keyword, inputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != outputString){
		errorString += testingError(codingState::ENCODE, inputString, outputString, cipherString);
		passed = false;
	}
	//This removes the spaces and turns everything capital so the reverse will work correctly
	inputString = cipher.getInputString();

	//Do the same thing in reverse
	cipher.reset();
	cipherString = cipher.decode(keyword, outputString);
	//If the cipher didn't spit out the correct string throw an error
	if(cipherString != inputString){
		errorString += testingError(codingState::DECODE, outputString, inputString, cipherString);
		passed = false;
	}

	return passed;
}
#endif //AUTOKEY_TEST

#endif //TEST_VERSION
