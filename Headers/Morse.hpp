//Ciphers/Headers/Morse.hpp
//Matthew Ellison
// Created: 5-1-18
//Modified: 5-16-18
//This file contains the declaration of the Morse class
//This class is designed to translate Morse Code into regular letters and numbers

#ifndef MORSE_HPP
#define MORSE_HPP

#include <string>
#include <sstream>


class Morse{
private:
	//Holds the Morse representation of the alphanumeric characters
	static const std::string code[];
	std::stringstream inputString;	//The string that needs encoded/decoded
	std::string outputString;		//The encoded/decoded message
	static const std::string version;	//The current library's version number
	std::string encode();	//Encodes inputString and stores the result in outputString
	std::string decode();	//Decodes inputString and stores the result in outputString
	void setEncodeInputString(std::string input);	//Encodes input and returns the result
	void setDecodeInputString(std::string input);	//Decodes input and returns the result
public:
	Morse();
	~Morse();
	std::string getInputString() const;		//Returns inputString
	std::string getOutputString() const;	//Returns outputString
	std::string encode(std::string input);	//Encodes input and returns the result
	std::string decode(std::string input);	//Decodes input and returns the result
	void reset();	//Makes sure all variables are empty
	static std::string getVersion();
};

#endif //MORSE_HPP