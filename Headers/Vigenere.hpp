//Ciphers/SourceFiles/Vigenere.hpp
//Matthew Ellison
// Created:	4-29-18
//Modified: 5-16-18
//This file contains the declaration of the Vigenere class

#ifndef VIGENERE_HPP
#define VIGENERE_HPP


#include <string>
#include <vector>


class Vigenere{
protected:
	class emptyKeyword{};	//A class to help with error handling
	std::string inputString;    //This is the string that you want to encode or decode
	std::string outputString;   //This is the string that is output from encoding or decoding
	std::string keyword;        //This is the keyword that is the resposible for determining the offsets that you change each character by
	static const std::string version;	//The current library's version number
	std::vector<unsigned int> offset;   //This holds the offsets computed from each character in the keyword
	void setOffset();	//Uses keyword to calculate the offset for the Caesar cipher for each character
	void setInputString(std::string input);	//Sets inputString
	void setKeyword(std::string key);	//Sets keyword
	std::string encode();	//Encodes inputString and stores the result in outputString
	std::string decode();	//Decoded inputString and stores the result in outputString
public:
	Vigenere();
	~Vigenere();
	std::string getInputString() const;		//Returns the current inputString
	std::string getOutputString() const;	//Returns the current outputString
	std::string getKeyword() const;			//Returns the current keyword
	std::vector<unsigned int> getOffsets() const;	//Returns the current offsets (Used mostly in bug fixing)
	std::string encode(std::string key, std::string input);	//Encodes input using key and returns the result
	std::string decode(std::string key, std::string input);	//Decodes input using key and returns the result
	void reset();	//Makes sure all of the variables are empty
	static std::string getVersion();
};

#endif //VIGENERE_HPP
