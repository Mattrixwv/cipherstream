//Ciphers/Headers/Autokey.hpp
//Matthew Ellison
// Created: 5-2-18
//Modified: 5-16-18
//This file contains the declaration of the Autokey class
//This class will encode a message using the Autokey cipher

#ifndef AUTOKEY_HPP
#define AUTOKEY_HPP

#include "Vigenere.hpp"


//TODO: Might be able to make encoding and decoding faster for longer messages by actually using a grid of alphabets (Caesar Ciphers)
class Autokey : public Vigenere{
protected:
	void encodeSet(std::string key, std::string input);	//Special rules for setting the strings for encoding
	void decodeSet(std::string key, std::string input);	//Setting the strings for decoding
	std::string decode();	//Decodes the inputString
public:
	Autokey();
	~Autokey();
	virtual std::string encode(std::string key, std::string input);	//Encodes inputString using the Autokey cipher
	virtual std::string decode(std::string key, std::string input);	//Decodes inputString using the Autokey cipher
	static std::string getVersion();
};


#endif //AUTOKEY_HPP
