//Ciphers/Headers/Atbash.hpp
//Matthew Ellison
// Created: 4-30-18
//Modified: 5-16-18
//This file contains the declaration of the Atbash class
//This class is used to encode and decode an Atbash cipher

#ifndef ATBASH_HPP
#define ATBASH_HPP

#include <string>


class Atbash{
private:
	std::string inputString;	//Holds the string that needs encoded or decoded
	std::string outputString;	//Holds the encoded/decoded string
	static const std::string version;	//Holds the current version of the library
	std::string decode();		//Decodes inputString and stores in outputString
	std::string encode();		//Encodes inputString and stores in outputString
	void setInputString(std::string input);	//Removes all invalid characters and sets inputString
public:
	Atbash();
	~Atbash();
	std::string getInputString() const;
	std::string getOutputString() const;
	std::string encode(std::string input);
	std::string decode(std::string input);
	void reset();
	static std::string getVersion();
};

#endif //ATBASH_HPP
