//Ciphers/Headers/Caesar.hpp
//Matthew Ellison
// Created: 4-25-18
//Modified: 5-16-18
//This file contains the declaration of the Caesar class
//This class implements the Caesar Cipher and is inteded to be turned into a library


#ifndef CAESAR_HPP
#define CAESAR_HPP

#include <string>

class Caesar{
private:
	std::string inputString;	//The string that needs encoded/decoded
	std::string outputString;	//The encoded/decoded string
	static const std::string version;	//The current version number for the library
	int shift;					//The amount that you need to shift each letter
	void setShift(int shiftAmount);	//Sets shift and makes sure it is within the propper bounds
	void setInputString(std::string inputString);	//Sets the input string
	std::string encode();	//Encodes the inputString and stores the result in outputString
	std::string decode();	//Decodes the inputString and stores the result in outputString
public:
	Caesar();
	~Caesar();
	std::string getInputString() const;	//Returns the inputString
	int getShift() const;	//Returns shift
	std::string getOutputString() const;	//Returns the outputString
	std::string encode(int shiftAmount, std::string input);	//Sets the shift and inputString and encodes the message
	std::string decode(int shiftAmount, std::string input);	//Sets the shift and inputString and decodes the message
	void reset();	//Makes sure all of the variables are empty
	static std::string getVersion();
};


#endif //CAESAR_HPP
