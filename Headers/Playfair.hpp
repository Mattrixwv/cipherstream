//Ciphers/Headers/Playfair.hpp
//Matthew Ellison
// Created: 4-25-18
//Modified: 5-16-18
//This file contains the declaration of the Playfair class
//It is designed to encrypt and decrypt strings using the Playfair cipher


#ifndef PLAYFAIR_HPP
#define PLAYFAIR_HPP

#include <string>


class Playfair{
private:
	//Classed to help with error handling
	class letterNotFound{
		private:
		char letter;
		public:
		letterNotFound(char letter): letter(letter)	{}
		char getLetter(){	return letter;	}
	};
	class invalidGrid{
		private:
		std::string type;
		unsigned int size;
		public:
		invalidGrid(std::string type, unsigned int size): type(type), size(size)	{}
		std::string getType(){	return type;	}
		int getSize(){	return size;	}
	};
	static char REPLACED;	//The letter that will need to be replaced in the grid and any input string or keyword
	static char REPLACER;	//The letter that replaced REPLACED in any input string or keyword
	static char DOUBLED;	//The letter that will be placed between double letters in the input string if necessary or to make the string length even
	std::string inputString;	//The message that needs to be encoded/decoded
	std::string outputString;	//The encoded/decoded message
	std::string keyword;		//The keyword used to create the grid
	static const std::string version;	//The current library's version number
	char grid[5][5];			//The grid used to encode/decode the message
	void createGrid();			//Create the grid from the keyword
	bool checkGrid(const char letter) const;			//Returns true if the letter is found in the grid
	void searchGrid(char letter, int& row, int& col);	//Searches the grid for letter and sets row & col to its location in grid
	void setInputString(std::string input);	//Strips invalid characters from the string that needs encoded/decoded
	void setKeyword(std::string key);	//Strips invalid character from the keyword and creates the grid
	std::string encode();	//Encodes inputString using the Playfair cipher and stores the result in outputString
	std::string decode();	//Decodes inputString using the Playfair cipher and stores the result in outputString
public:
	Playfair();
	~Playfair();
	std::string encode(std::string keyword, std::string input);	//Sets the keyword and inputString and encodes the message
	std::string decode(std::string keyword, std::string input);	//Sets the keyword and inputString and decodes the message
	std::string getKeyword() const;			//Returns the keyword used to create the grid
	std::string getInputString() const;		//Returns the string that needs encoded/decoded
	std::string getOutputString() const;	//Returns the encoded/decoded message
	std::string getGrid() const;			//Returns a string representation of the grid
	void reset();	//Makes sure all variables are empty
	//Change static variables
	static char getReplaced();	//Returns the character that needs replaced in messages and the grid
	static char getReplacer();	//Returns the character that replaces the character that needs replaced
	static char getDoubled();	//Returns the character that is added between 2 adjacent characters that are the same
	static void setReplaced(const char replaced);	//Sets the character that needs replaced in messages and the grid
	static void setReplacer(const char replacer);	//Sets the character that replaces the character that needs replaced
	static void setDoubled(const char doubled);		//Sets the character that is added betwee 2 adjacent characters that are the same
	static std::string getVersion();
};

#endif //PLAYFAIR_HPP
