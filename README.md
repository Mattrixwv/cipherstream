#CipherStream
This is a program to encode and decode some simple ciphers.
This program is designed to allow you to play with some simple ciphers, like the Caesar Cipher.
Because of how simple these ciphers are they will likely not be of any use if you actually want to keep your data safe.
This program and all libraries in this project are licensed under the GNU General Public License.

#Caesar Cipher
This cipher is very easy to learn and simple to encode or decode.
It just shifts the letters a certain number of spaces, i.e. for a shift of 3 a=d & x=a

#Vigenere Cipher
This cipher is more complicated than a simple Caesar cipher.
It uses an array of Caesar ciphers and cycles between them based on a key.
This makes it more secure, but if you are not carful in your message or have a short key it will still output recognizable paterns that can be easily broken.

#Autokey Cipher
The Autokey cipher is very similar to the Vigenere cipher, but with a more secure key.
Instead of having a repeated key, the key is used one time and the message itself is used as the rest of the key, out to the length of the message.
This makes it more secure, but also slower to decrypt.

#Atbash Cipher
The Atbash cipher is a simple cipher that basically reverses the alphabet.
A=Z, B=Y, etc.

#Playfair Cipher
This cipher is a little more complex.
It uses a key to create a grid of letters that is then used to encode 2 letters at a time. It is a simple form of 16-bit encryption.
It is still fairly simple to learn to do by hand, just time consuming if it is a long message or if you are trying to crack the key.

#Morse Code
This is technically not encryption, but it is not exactly English either.
It was used originally on the telegraph and was considered an efficient way to send messages at that time.
The dots and dashes are representative of long and short pulses from a speaker.
