#CipherStream/makefile
#Matthew Ellison
# Created: 4-25-18
#Modified: 9-19-18
#	Copyright (C) 2018  Matthew Ellison
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.

#A list of the ciphers that can be used
#This is used to create the libraries and for cpp files in the test version
CIPHERS = Caesar Playfair Vigenere Atbash Morse Autokey
#Other usefull macros
LIBFLAGS = -shared -std=c++11 -O3 -fPIC -Wall
EXEFLAGS = -std=c++11 -O3 -Wall -Wl,-rpath,'$$ORIGIN/lib'
TESTFLAGS = -std=c++11 -DTEST_VERSION -Wl,-rpath,'$$ORIGIN/lib'
DEBUGFLAGS = $(TESTFLAGS) -g
LIBDIR = ./lib
LIBFILES = $(patsubst %,SourceFiles/%.cpp,$(CIPHERS))

#Different portions to compile
#Using the simplest make command compiles all
all: libs CipherStream
test: libs CiphersTest
debug: CiphersDBG
libs: directory $(patsubst %, $(LIBDIR)/lib%.so,$(CIPHERS))
LIBS = $(patsubst %, -l%,$(CIPHERS))

directory:
	mkdir -p $(LIBDIR)


#Building Libraries
$(LIBDIR)/lib%.so: SourceFiles/%.cpp
	$(CXX) $(LIBFLAGS) -o $@ $<

#Building Executables
CipherStream: main.cpp helperFunctions.hpp
	$(CXX) $(EXEFLAGS) -o $@ main.cpp -L $(LIBDIR) $(LIBS)

CiphersTest: main.cpp
	$(CXX) $(TESTFLAGS) -o $@ $< -L $(LIBDIR) $(LIBS)


#Cleaning Shop
.PHONY: clean

#Linux Remove
clean:
	rm -f lib/*.so Cipher*
