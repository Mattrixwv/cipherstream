//Ciphers/SourceFiles/Caesar.hpp
//Matthew Ellison
// Created: 4-25-18
//Modified: 5-16-18
//This file contains the implementation of the Caesar class
//This class implements the Caesar Cipher and is inteded to be turned into a library


#include "../Headers/Caesar.hpp"
#include <string>
#include <cctype>


/**
 * @brief The current library's version number
 * 
 */
const std::string Caesar::version = "1.0";

/**
 * @brief Construct a new Caesar:: Caesar object
 * 
 */
Caesar::Caesar(){
	reset();
}

/**
 * @brief Destroy the Caesar:: Caesar object
 * 
 */
Caesar::~Caesar(){
}

/**
 * @brief Sets the current inputString
 * 
 * @param input The string that you wish to encode
 */
void Caesar::setInputString(std::string input){
	inputString = input;
}

/**
 * @brief Returns the current inputString
 * 
 * @return The message that was just encoded/decoded
 */
std::string Caesar::getInputString() const{
	return inputString;
}

/**
 * @brief Set shift to a valid integer
 * 
 * @param shiftAmount The number of letters the cipher needs to be shifted
 */
void Caesar::setShift(int shiftAmount){
	//If you shift more than 26 you will just be wrapping back around again
	shift = shiftAmount % 26;
}

/**
 * @brief Returns the current number of letters the cipher is set to shift
 * 
 * @return The current number of letters the cipher is currently set to shift
 */
int Caesar::getShift() const{
	return shift;
}

/**
 * @brief Returns the last message encoded/decoded
 * 
 * @return The messge that was just encoded/decoded
 */
std::string Caesar::getOutputString() const{
	return outputString;
}

/**
 * @brief Encodes a message using the Caesar cipher
 * 
 * @return The encoded message (outputString)
 */
std::string Caesar::encode(){
	unsigned char temp;	//A temperary holder for the current working character
	for(unsigned int cnt = 0;cnt < inputString.size();++cnt){
		temp = inputString.at(cnt);
		//If it is a upper case letter shift it and wrap if necessary
		if(isupper(temp)){
			temp += shift;
			//Wrap around if the letter is now out of bounds
			if(temp < 'A'){
				temp += 26;
			}
			else if(temp > 'Z'){
				temp -= 26;
			}
		}
		//If it is a lower case letter shift it and wrap if necessary
		else if(islower(temp)){
			temp += shift;
			//Wrap around if the letter is now out of bounds
			if(temp < 'a'){
				temp += 26;
			}
			else if(temp > 'z'){
				temp -= 26;
			}
		}
		//If it is whitespace, number, or punctuation just let it pass through
		//Add it to the output string
		outputString += temp;
	}
	return outputString;
}

/**
 * @brief Encodes a message using the Caesar cipher
 * 
 * @param shiftAmount The number of letters you need to shift for the cipher
 * @param input The message that needs encoded
 * @return The encoded message
 */
std::string Caesar::encode(int shiftAmount, std::string input){
	reset();
	setShift(shiftAmount);
	setInputString(input);
	return encode();
}

/**
 * @brief Decodes a message using the Caesar cipher
 * 
 * @return The decoded message (outputString)
 */
std::string Caesar::decode(){
	unsigned char temp;
	for(unsigned int cnt = 0;cnt < inputString.size();++cnt){
		temp = inputString.at(cnt);
		//If it is an upper case letter shift it and wrap if necessary
		if(isupper(temp)){
			temp -= shift;
			if(temp < 'A'){
				temp += 26;
			}
			else if(temp > 'Z'){
				temp -= 26;
			}
		}
		//If it is a lower case letter shift it and wrap if necessary
		else if(islower(temp)){
			temp -= shift;
			if(temp < 'a'){
				temp += 26;
			}
			else if(temp > 'z'){
				temp -= 26;
			}
		}
		//If it is whitespace, number, or punctuation just let it pass through
		//Add it to the output string
		outputString += temp;
	}
	return outputString;
}

/**
 * @brief Decodes a message using the Caesar cipher
 * 
 * @param shiftAmount The number of letters you need to shift for the cipher
 * @param input The message that needs decoded
 * @return The decoded message
 */
std::string Caesar::decode(int shiftAmount, std::string input){
	reset();
	setShift(shiftAmount);
	setInputString(input);
	return decode();
}

/**
 * @brief Makes sure all of the variables are blank
 * 
 */
void Caesar::reset(){
	inputString = outputString = "";
	shift = 0;
}

/**
 * @brief Returns a string containing the version information
 * 
 * @return The version information
 */
std::string Caesar::getVersion(){
	return version;
}
