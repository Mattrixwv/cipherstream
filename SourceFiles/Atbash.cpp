//Ciphers/SourceFiles/Atbash.cpp
//Matthew Ellison
// Created: 4-30-18
//Modified: 5-16-18
//This file contains the implementation of the Atbash class


#include "../Headers/Atbash.hpp"
#include <cctype>


/**
 * @brief The current library's version number
 * 
 */
const std::string Atbash::version = "1.0";

/**
 * @brief Construct a new Atbash object
 *
 */
Atbash::Atbash(){
}

/**
 * @brief Destroy the Atbash object
 * 
 */
Atbash::~Atbash(){
}

/**
 * @brief Strips all invalid characters from input and sets it to inputString
 * 
 * @param input The string that you wish to encode or decode
 */
void Atbash::setInputString(std::string input){
	//Strip all punctuation and whitespace from input and make all letters capital
	for(unsigned int cnt = 0;cnt < input.size();++cnt){
		char letter = input[cnt];
		//If it is upper case add it to the inputString
		if(isupper(letter)){
			inputString += letter;
		}
		//If it is lower case make it upper case and add it to the inputString
		else if(islower(letter)){
			inputString += toupper(letter);
		}
		//If it is anything else ignore it
	}
}

/**
 * @brief Returns the stripped inputString
 * 
 * @return The current inputString
 */
std::string Atbash::getInputString() const{
	return inputString;
}

/**
 * @brief Returns the current outputString
 * 
 * @return The current outputString
 */
std::string Atbash::getOutputString() const{
	return outputString;
}

/**
 * @brief Uses the Atbash cipher to encode inputString and store it in outputString
 * 
 * @return The encoded inputString (outputString)
 */
std::string Atbash::encode(){
	//Step through every element in the inputString and shift it the correct amount
	for(unsigned int cnt = 0;cnt < inputString.size();++cnt){
		outputString += (inputString[cnt] + 25 - (2 * (inputString[cnt] - 'A')));
	}

	return outputString;
}

/**
 * @brief A simple interface that makes sure inputString is set and encodes it using the Atbash cipher
 * 
 * @param input The string that you wish to encode
 * @return The encoded string
 */
std::string Atbash::encode(std::string input){
	//Make sure everything is empty before you begin
	reset();
	setInputString(input);
	return encode();
}

/**
 * @brief Uses the Atbash cipher to decode inputString and store it in outputString
 * 
 * @return The decoded inputString (outputString)
 */
std::string Atbash::decode(){
	for(unsigned int cnt = 0;cnt < inputString.size();++cnt){
		outputString += (inputString[cnt] + 25 - (2 * (inputString[cnt] - 'A')));
	}

	return outputString;
}

/**
 * @brief A simple interface that makes sure inputString is set and decodes it using the Atbash cipher
 * 
 * @param input The string that you wish to encode
 * @return The decoded string
 */
std::string Atbash::decode(std::string input){
	//Make sure everything is empty before you begin
	reset();
	setInputString(input);
	return decode();
}

/**
 * @brief Resets all variables
 * 
 */
void Atbash::reset(){
	inputString = outputString = "";
}

/**
 * @brief Returns a string containing the version information
 * 
 * @return The version information
 */
std::string Atbash::getVersion(){
	return version;
}
