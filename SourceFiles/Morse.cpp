//Ciphers/SourceFiles/Morse.cpp
//Matthew Ellison
// Created: 5-1-18
//Modified: 5-16-18
//This file contains the implementation of the Morse class


#include "../Headers/Morse.hpp"
#include <string>
#include <cctype>


/**
 * @brief The current library's version number
 * 
 */
const std::string Morse::version = "1.0";

/**
 * @brief The dot-dash representation of the alphabet and numbers
 * 
 */
const std::string Morse::code[] {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", //A-L
		"--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", //M-Z
		"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."}; //0-9

/**
 * @brief Construct a new Morse object
 * 
 */
Morse::Morse(){
	reset();
}

/**
 * @brief Destroy the Morse object
 * 
 */
Morse::~Morse(){
}

/**
 * @brief Uses alphabet rules to set inputString
 * 
 * @param input The string to be encoded
 */
void Morse::setEncodeInputString(std::string input){
	//Make sure the input is empty to begin
	std::string temp;
	//Step through every element in input, removing anything that is not a letter and making all letters uppercase
	for(unsigned int cnt = 0;cnt < input.size();++cnt){
		char letter = input[cnt];
		//If it is a letter or number add it to the inputString, turning all letters capital
		if(isalnum(letter)){
			temp += toupper(letter);
		}
		//If letter is not a letter or number ignore it (This removes punctuation and whitespace)
	}
	inputString.clear();
	inputString.str(temp);
}

/**
 * @brief Uses . & - rules to set inputString
 * 
 * @param input The string that needs decoded
 */
void Morse::setDecodeInputString(std::string input){
	std::string temp;
	//Step through every element in input, removing everything except . or -
	for(unsigned int cnt = 0;cnt < input.size();++cnt){
		char letter = input[cnt];
		//If it is a letter or number add it to the inputString, turning all letters capital
		if(letter == '.' || letter == '-' || letter == ' '){
			letter = toupper(letter);
			temp += letter;
		}
		//If letter is not a letter or number ignore it (This removes punctuation and whitespace)
	}
	inputString.clear();
	inputString.str(temp);
}

/**
 * @brief Returns the string that needs encoded/decoded
 * 
 * @return The string that needs encoded/decoded
 */
std::string Morse::getInputString() const{
	return inputString.str();
}

/**
 * @brief Returns the encoded/decoded message
 * 
 * @return The encoded/decoded message
 */
std::string Morse::getOutputString() const{
	return outputString;
}

/**
 * @brief Encodes inputString and stores the result in outputString
 * 
 * @return The encoded message
 */
std::string Morse::encode(){
	outputString = "";
	std::string temp = inputString.str();
	//Loop through every element in the input string and see what type it is
	//while(!inputString.eof()){
	for(unsigned int cnt = 0;cnt < temp.size();++cnt){
		char letter;
		//Get the next character in the input
		letter = temp[cnt];
		//If it is a letter get the correct combination from code and add it to the outputString
		if(isupper(letter)){
			outputString += code[letter - 65] + " ";
		}
		//If it is a number get the correct combination from code and add it to the outputString
		else if(isdigit(letter)){
			std::string temp = "";
			temp += letter;
			outputString += code[std::stoi(temp) + 26] + " ";
		}
	}
	//Remove the final space from the output
	if(outputString.size() > 0){
		outputString.erase(outputString.end() - 1);
	}

	return outputString;
}

/**
 * @brief Encodes the message contained in input
 * 
 * @param input The message that needs encoded
 * @return The encoded message
 */
std::string Morse::encode(std::string input){
	setEncodeInputString(input);
	return encode();
}

/**
 * @brief Decodes inputString and stores the result in outputString
 * 
 * @return The decoded message
 */
std::string Morse::decode(){
	outputString = "";
	//Loop until you reach the end of the input
	while(!inputString.eof()){
		std::string current;
		bool found = false;
		//Get the next string of characters from the code
		inputString >> current;
		//Loop through code and see if current is a letter
		for(int cnt = 0;(cnt < 26) && (!found);++cnt){
			//See if current is the same as an element in code
			if(current == code[cnt]){
				//Add 65 to cnt to get the correct capital letter
				outputString += (char)(65 + cnt);
				found = true;
			}
		}
		//Loop through code and see if current is a number
		for(int cnt = 26;(cnt < 36) && (!found);++cnt){
			if(current == code[cnt]){
				//Remove 26 from cnt to get the correct number
				outputString += std::to_string((cnt - 26));
				found = true;
			}
		}
		//If it is neither print an error in the output
		if(!found){
			outputString += "<Unknown symbol: " + current + ">";
		}
	}

	return outputString;
}

/**
 * @brief Decodes the message contained in input
 * 
 * @param input The message that needs decoded
 * @return The decoded message
 */
std::string Morse::decode(std::string input){
	setDecodeInputString(input);
	return decode();
}

/**
 * @brief Makes sure all variables are blank
 * 
 */
void Morse::reset(){
	inputString.str("");
	outputString = "";
}

/**
 * @brief Returns a string containing the version information
 * 
 * @return The version information
 */
std::string Morse::getVersion(){
	return version;
}
