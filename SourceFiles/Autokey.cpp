//Ciphers/SourceFiles/Autokey.cpp
//Matthew Ellison
// Created: 5-3-18
//Modified: 5-16-18
//This file contains the implementation of the Autokey class


#include "../Headers/Autokey.hpp"
#include <string>


/**
 * @brief The current library's version number
 * 
 */
const std::string Vigenere::version = "1.0";

/**
 * @brief Construct a new Autokey object
 * 
 */
Autokey::Autokey(){
	reset();
}

/**
 * @brief Destroy the Autokey object
 * 
 */
Autokey::~Autokey(){
}

/**
 * @brief Sets the strings correctly for encoding
 * 
 * @param key The keyword used for the cipher
 * @param input The string that you wish to encode
 */
void Autokey::encodeSet(std::string key, std::string input){
	//Remove all unneccessary elements from the key
	Vigenere::setKeyword(key);
	key = Vigenere::getKeyword();
	//Remove all unneccessary elements from the input
	Vigenere::setInputString(input);
	input = getInputString();
	key.append(input);

	//This will take a long time if the keyword in long
	//Remove the last letter in the string until it is the same size as the input
	while(key.size() > input.size()){
		key.erase(key.end() - 1);
	}

	//Set the new Keyword
	keyword = key;
	//Make sure to update the offset
	offset.clear();
	setOffset();
}

/**
 * @brief Sets the strings correctly for decoding
 * 
 * @param key The keyword used for the cipher
 * @param input The string that you wish to decode
 */
void Autokey::decodeSet(std::string key, std::string input){
	//Remove all unneccessary elements from the key
	Vigenere::setKeyword(key);
	//Remove all unneccessary elements from the input
	inputString = "";
	Vigenere::setInputString(input);
}

/**
 * @brief Decodes the message stored in inputString
 * 
 * @return The decoded string
 */
std::string Autokey::decode(){
	//This is going to take a long time no matter what
	//You must decode what you can and then add the decoded portion to the key and do it all over again
	//std::string input;
	std::string output;
	std::string key = keyword;
	unsigned int keyLength = keyword.size();
	for(int cnt = 0;(key.size() * cnt) < inputString.size();++cnt){
		outputString = "";
		//Make sure you are not going to run out of bounds with the keyword
		//This also allows the key to be longer than the message
		if(keyword.size() == keyLength){
			while(keyLength > inputString.size()){
				keyword.erase(keyword.end() - 1);
			}
		}
		else if(keyword.size() > inputString.size()){
			while(keyword.size() > inputString.size()){
				keyword.erase(keyword.end() - 1);
			}
		}
		else{
			while((keyword.size() + keyLength) > inputString.size()){
				//key.erase(key.end() - 1);
				--keyLength;
			}
		}

		//Decode the portion that you have
		Vigenere::decode();
		std::string temp = outputString.substr(cnt * key.size(), keyLength);
		output += temp;
		keyword += temp;
		//Make sure to update the offsets
		offset.clear();
		setOffset();
	}

	return outputString;
}

/**
 * @brief Encodes a message using the Autokey cipher
 * 
 * @param key The keyword used for the cipher
 * @param input The message that needs to be encoded
 * @return The encoded message
 */
std::string Autokey::encode(std::string key, std::string input){
	reset();
	encodeSet(key, input);
	return Vigenere::encode();
}

/**
 * @brief Decodes a message using the Autokey cipher
 * 
 * @param key The keyword used for the cipher
 * @param input The message that needs to be decoded
 * @return The decoded message
 */
std::string Autokey::decode(std::string key, std::string input){
	reset();
	setInputString(input);
	decodeSet(key, input);	//Decoding is a bit different because part of the key is also part of the original message
	return Autokey::decode();
}

/**
 * @brief Returns a string containing the version information
 * 
 * @return The version information
 */
std::string Autokey::getVersion(){
	return version;
}
